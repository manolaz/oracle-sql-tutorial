-- 1.1. Check to see if a student enrolled into courses
-- for the other department than his/her own department.

select *
from student s,
     takes t,
     course c
where s.ID = t.ID
    and t.course_id = c.course_id
    and s.dept_name != c.dept_name -- 1.2. Make a trigger to check the question 1.1when registering a student a class.
 /*
Create a function to check relation between student department and course department
*/
    create or replace function check_takes_other_dept(p_student_id varchar2, p_course_id varchar2) return int as student_dept varchar2(200);

course_dept varchar2(200);

begin
select dept_name into student_dept
from student
where student.id = p_student_id;


select dept_name into course_dept
from course
where course.course_id = p_course_id;

if (student_dept = course_dept) then return 1;

else return 0;

end if;

end;

/*
Create a trigger to check depart relation when student register a class
*/
Create or replace trigger takes_other_dept_trig
before
insert
or
update on takes
for each row begin if (check_takes_other_dept(:NEW.id, :NEW.course_id)=0) THEN raise_application_error(-20001, 'Student department is different to course department'); end if; end;

/*
Test trigger
*/
insert into takes (id, course_id, sec_id, semester, year)
values ('98765',
        'CS-347',
        1,
        'Fall',
        2009);

/*
Test checking department relation
*/
select check_takes_other_dept('45678','CS-101')
from dual;

/*
:+1: 2 7:29 1.1.
Create a new table building
from the table classroom,
               that consists of building (name)
and capacity (sum of all room capacities in the building),
and populate the table by collecting data
from the current data.
*/
create table building as
    (select building,
            sum(capacity) as capacity
     from classroom
     group by building);


select *
from building;

-- 7:30 1.1. Make references (
--                            foreign key)
from tables classroom
and department to the new created table building.
alter table building add constraint building_PK primary key (building) enable;


alter table classroom add constraint bulding_FK
foreign key (building) references building (building);


alter table department add constraint building_FK
foreign key (building) references building (building);

-- 7:30 1.3.
 -- List all instructors,
--                    their department,
-- and the number of teaching hours
-- for each semester. Assume that,
--          the Spring
-- and Fall semester are in 15 weeks
-- and the Summer is in 10 weeks

select ins.ID,
       ins.name,
       ins.dept_name,
       s.year,
       s.semester,
       sum (((ts.end_hr - ts.start_hr)*60 + (ts.end_min - ts.start_min))/50*15) as tot_hrs
from instructor ins
inner join teaches te on ins.id = te.id
inner join section s on te.course_id = s.course_id
and te.sec_id = s.sec_id
and te.semester = s.semester
and te.year = s.year
inner join time_slot ts on s.time_slot_id = ts.time_slot_id
where te.semester in ('Fall',
                      'Spring')
group by s.year,
         s.semester,
         ins.dept_name,
         ins.ID,
         ins.name
union
select ins.ID,
       ins.name,
       ins.dept_name,
       s.year,
       s.semester,
       sum (((ts.end_hr - ts.start_hr)*60 + (ts.end_min - ts.start_min))/50*10) as tot_hrs
from instructor ins
inner join teaches te on ins.id = te.id
inner join section s on te.course_id = s.course_id
and te.sec_id = s.sec_id
and te.semester = s.semester
and te.year = s.year
inner join time_slot ts on s.time_slot_id = ts.time_slot_id
where te.semester = 'Summer'
group by s.year,
         s.semester,
         ins.dept_name,
         ins.ID,
         ins.name /*
1.3. Create a new table buildingfromthe table classroom,
that consists of building (name
) and capacity (sum of all room capacities in the building),
and populate the table by collecting data from the current data.
1.4. Make references (foreign key) from tables classroom
and department to the new created table building.
1.5. List all instructors, their department, and the number
of teaching hoursfor each semester. Assume that, the Spring and
Fall semester are in15 weeksand the Summer is in
10 weeks.Program: Master Informatics –Software EngineeringClass:
MINF18Course: Advanced Database -TDLevel: M1Lecturer: Bui Hoai ThangAcademic
Year: 2018-2019Semester: 2Session: 1Date: .../.../2019Duration:120 minutes
*/