-- Part B.
/*
New databaseschemaExtend the PUF university schema
for capturing the student activities in the university:
There are many events organized by some student clubs in university.
Assume that, the schema is as follows.activity(activity_id, activity_name,
activity_description, activity_location, activity_start_date, activity_end_date,
activity_price, activity_limit, activity_joined)activity_participation
(activity_id, student_id,activity_role)activity_role
(activity_role, activity_role_name, discount)
Primary keys:activity: activity_idactivity_role:
activity_roleactivity_participation: activity_id,
student_idForeign keys:activity_paticipation.
activity_id →activity.activity_idactivity_paticipation.
student_id →student.idactivity_paticipation.
activity_role →activity_role.activity_roleConstraints:
•All activities must have name,description,
start and end dates, and location•activity.activity_price is positive
and cannot exceed 999,999.00•activity_role.discount is a
decimal number and should be from 0 to
*/ /*
 12.1. Make a script file to create a relational database for the system.
See the queries below for the datatypes of all the columns.

*/ /*
2.2. Make a script file to initialize (to populate) the database.
There are at least 5 activities; 3 roles; 5 students join activities,
some of them join more than 1 activities.
*/ /*
2.3. Write a query to calculate the billfor a student in a month
 for all activities started in that month.
Remember to use for the discounts if any.
*/ /*
2.4. List all activities that Adam Sadler joined except
those that are joined by Mary Janes.
*/ /*
2.5. List the activities and participated students that
ended within the current month.
*/ /*
2.6.  Check  to  see  if  there  is  any  mistake  on
theactivityregistration:  the  number  of participations
is more that the activitylimitation (activity.activity_limit).
*/ /*
2.7. Update the number of registration for all activities
(to activity.activity_joined).
*/ /*
2.8.  Make a trigger to update the number  of registration for
an activitywhenever a student joins that activity.
Hint: before / after insert trigger on table activity_participation
*/