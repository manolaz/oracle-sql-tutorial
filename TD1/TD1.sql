/* 
First TD lab - 14 Jul 2020
*/

-- 1. List all courses
SELECT *
FROM course

-- 2. List 4-credit courses 
SELECT *
FROM course
WHERE credits = 4

-- 3. List 4-credit courses of the Biology department
SELECT *
FROM course
WHERE credits = 4
  AND dept_name = 'Biology'
  
-- 4. List courses of the Biology or Computer Science department,
-- and rename the column dept_name into department
SELECT course_id, title, dept_name AS department, credits
FROM course
WHERE dept_name = 'Biology'
  OR dept_name = 'Comp. Sci.'

-- 5. List all courses, order by credit
SELECT *
FROM course
ORDER BY credits DESC

SELECT course_id, title, dept_name AS department, credits
FROM course
ORDER BY department


-- 6. List all employees from the SCOTT schema
SELECT *
FROM scott.emp

-- 7. List all employees from the SCOTT schema who have joined the company in 1987
SELECT *
FROM scott.emp
WHERE hiredate BETWEEN '01-JAN-87' AND '31-DEC-87'

SELECT * 
FROM scott.emp
WHERE EXTRACT(YEAR FROM hiredate) = 1987


-- 8. List all employees from the SCOTT schema with the hired year
-- and arrange them by the hired year 
SELECT scott.emp.*, EXTRACT(YEAR FROM hiredate) AS hireyear
FROM scott.emp
ORDER BY EXTRACT(YEAR FROM hiredate)

-- table alias
SELECT scott_emp.*, EXTRACT(YEAR FROM scott_emp.hiredate) AS hireyear
FROM scott.emp scott_emp
ORDER BY EXTRACT(YEAR FROM scott_emp.hiredate)

-- table alias
SELECT A.*, EXTRACT(YEAR FROM A.hiredate) AS hireyear
FROM scott.emp A
ORDER BY EXTRACT(YEAR FROM A.hiredate)

-- 9. Show the min/max/average salary and the total salary in the SCOTT schema
SELECT MIN(sal) AS min_sal, MAX(sal) AS max_sal, SUM(sal) AS total_sal
FROM scott.emp

-- 10. Show the employee who has the min salary
SELECT a.*
FROM scott.emp a
WHERE a.sal = (SELECT MIN(b.sal)
                FROM scott.emp b)

-- 11. Show the employee who has the min salary for department no 10
SELECT a.*
FROM scott.emp a
WHERE a.sal = (SELECT MIN(b.sal)
                FROM scott.emp b
                WHERE b.deptno = 10)
    AND a.deptno = 10
    
-- 12. Show the employee who has the min salary for each department

SELECT a.*
FROM scott.emp a
WHERE a.sal = (SELECT MIN(b.sal)
                FROM scott.emp b
                WHERE b.deptno = a.deptno)

-- 13. Show the employee who has the min or max salary
SELECT *
FROM scott.emp
WHERE sal = (SELECT MIN(sal) as min_sal
            FROM scott.emp)
    OR sal = (SELECT MAX(sal) as min_sal
            FROM scott.emp)
            

SELECT empno, ename, sal, 'lowest salary guy in the company' as notes
FROM scott.emp
WHERE sal = (SELECT MIN(sal) as min_sal
            FROM scott.emp)
UNION
SELECT empno, ename, sal, 'highest salary guy in the company'
FROM scott.emp
WHERE sal = (SELECT MAX(sal) as min_sal
            FROM scott.emp)
UNION 
SELECT 1111, 'Fake guy', 0, 'no idea'
FROM dual
ORDER BY 3 DESC


-- 14. Show the min/max/average salary and the total salary in the SCOTT schema
-- for each department that has more than three employees
SELECT deptno, COUNT(*) AS no_emp, MIN(sal) AS min_sal, MAX(sal) AS max_sal, SUM(sal) AS total_sal
FROM scott.emp
GROUP BY deptno
HAVING COUNT(*) > 3

-- 15. Search employees who have a 'T' in the name
SELECT *
FROM scott.emp
WHERE ename LIKE '%T%'

-- 16. Search employees whoes name ended by 'E'
SELECT *
FROM scott.emp
WHERE LOWER(ename) LIKE '%e'