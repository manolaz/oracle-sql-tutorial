/*
    The second TD lab
*/
-- 1. Show instructors who have classes in a year

SELECT DISTINCT lec.*
FROM instructor lec,
    teaches te,
    section sec
WHERE
    lec.id = te.id
    AND te.course_id = sec.course_id
    AND te.sec_id = sec.sec_id
    AND te.semester = sec.semester
    AND te.year = sec.year
    AND sec.year = 2009
    
SELECT DISTINCT lec.*
FROM instructor lec
    INNER JOIN teaches te ON lec.id = te.id
    INNER JOIN section sec ON te.course_id = sec.course_id
                                AND te.sec_id = sec.sec_id
                                AND te.semester = sec.semester
                                AND te.year = sec.year
WHERE
    sec.year = 2009

SELECT DISTINCT lec.*
FROM instructor lec
    INNER JOIN teaches te ON lec.id = te.id
WHERE
    te.year = 2009

SELECT DISTINCT lec.*
FROM instructor lec
WHERE id IN (
        SELECT te.id
        FROM teaches te
        WHERE
            te.year = 2009
        )

SELECT DISTINCT lec.*
FROM instructor lec
WHERE EXISTS (
        SELECT 1
        FROM teaches te
        WHERE
            te.id = lec.id
            AND te.year = 2009
        )

-- 2. Show instructors who have no class in a year
SELECT DISTINCT lec.*
FROM instructor lec
WHERE id NOT IN (
        SELECT te.id
        FROM teaches te
        WHERE
            te.year = 2009
        )


SELECT DISTINCT lec.*
FROM instructor lec
WHERE NOT EXISTS (
        SELECT 1
        FROM teaches te
        WHERE
            te.id = lec.id
            AND te.year = 2009
        )

SELECT DISTINCT lec.*
FROM instructor lec
MINUS
SELECT DISTINCT lec.*
FROM instructor lec
WHERE id IN (
        SELECT te.id
        FROM teaches te
        WHERE
            te.year = 2009
        )

-- 3. Show lecturers who have classes in two consecutive 

-- 3.1. Try to find all cases
SELECT * 
FROM teaches t1
    INNER JOIN teaches t2 ON t1.ID = t2.ID
WHERE (t1.year = t2.year AND t1.semester = 'Spring' and t2.semester = 'Summer')
    OR (t1.year = t2.year AND t1.semester = 'Summer' and t2.semester = 'Fall')
    OR (t1.year+1 = t2.year AND t1.semester = 'Fall' and t2.semester = 'Spring')    
ORDER BY t1.ID, t1.year, t1.semester


/*
insert into section (course_id, sec_id, semester, year, building, room_number, time_slot_id)
values ('CS-101', 1, 'Summer', 2010, 'Painter', 514, 'A')

insert into section (course_id, sec_id, semester, year, building, room_number, time_slot_id)
values ('BIO-301', 1, 'Fall', 2010, 'Painter', 514, 'A')

insert into teaches (id, course_id, sec_id, semester, year)
values (45565, 'CS-101', 1, 'Summer', 2010)

insert into teaches (id, course_id, sec_id, semester, year)
values (76766, 'BIO-301', 1, 'Fall', 2010)

*/

-- 3.2. Get the instructors
SELECT *
FROM instructor
WHERE id IN (
    SELECT t1.id
    FROM teaches t1
        INNER JOIN teaches t2 ON t1.ID = t2.ID
    WHERE (t1.year = t2.year AND t1.semester = 'Spring' and t2.semester = 'Summer')
        OR (t1.year = t2.year AND t1.semester = 'Summer' and t2.semester = 'Fall')
        OR (t1.year+1 = t2.year AND t1.semester = 'Fall' and t2.semester = 'Spring')    
    )

-- 3.3. Get the instructors with details
SELECT * 
FROM instructor t
    INNER JOIN teaches t1 ON t.ID = t1.ID
    INNER JOIN teaches t2 ON t1.ID = t2.ID
WHERE (t1.year = t2.year AND t1.semester = 'Spring' and t2.semester = 'Summer')
    OR (t1.year = t2.year AND t1.semester = 'Summer' and t2.semester = 'Fall')
    OR (t1.year+1 = t2.year AND t1.semester = 'Fall' and t2.semester = 'Spring')    
ORDER BY t1.ID, t1.year, t1.semester

-- 3.4. Short details
SELECT DISTINCT ins.id, ins.name, t1.year, t1.semester, t2.year, t2.semester
FROM instructor ins
    INNER JOIN teaches t1 ON ins.ID = t1.ID
    INNER JOIN teaches t2 ON t1.ID = t2.ID
WHERE (t1.year = t2.year AND t1.semester = 'Spring' and t2.semester = 'Summer')
    OR (t1.year = t2.year AND t1.semester = 'Summer' and t2.semester = 'Fall')
    OR (t1.year+1 = t2.year AND t1.semester = 'Fall' and t2.semester = 'Spring')    
ORDER BY ins.ID, t1.year, t1.semester

SELECT ins.id, ins.name, te.*
FROM instructor ins
    INNER JOIN (
            SELECT DISTINCT t1.id, t1.semester as p_semester, t1.year as p_year, t2.semester as semester, t2.year as year
            FROM teaches t1
                INNER JOIN teaches t2
                    ON t1.id = t2.id
            WHERE ((t1.semester = 'Fall' AND t2.semester = 'Spring' AND t1.year + 1 = t2.year)
                OR (t1.semester = 'Spring' AND t2.semester = 'Summer' AND t1.year = t2.year)
                OR (t1.semester = 'Summer' AND t2.semester = 'Fall' AND t1.year = t2.year))
            ) te
        ON ins.id = te.id

SELECT ins.id, ins.name, te.*
FROM instructor ins
    INNER JOIN (
            SELECT DISTINCT t1.id, t1.semester as p_semester, t1.year as p_year, t2.semester as semester, t2.year as year
            FROM teaches t1
                INNER JOIN teaches t2
                    ON t1.id = t2.id
                        AND ((t1.semester = 'Fall' AND t2.semester = 'Spring' AND t1.year + 1 = t2.year)
                            OR (t1.semester = 'Spring' AND t2.semester = 'Summer' AND t1.year = t2.year)
                            OR (t1.semester = 'Summer' AND t2.semester = 'Fall' AND t1.year = t2.year))
            ) te
        ON ins.id = te.id

        
-- 4. Display the timetable for a student in a semester

-- 4.1. Student timetable
SELECT s.id AS studentID, s.name AS studentName,
        s.year, s.semester, 
        ts.day, 
        ts.start_hr || ':' || trim(to_char(ts.start_min, '00')) || ' - ' ||
                ts.end_hr || ':' || trim(to_char(ts.end_min, '00')) AS time_slot,
        r.building, r.room_number,
        c.course_id, c.title AS course_title
FROM student s
    INNER JOIN takes tk ON s.id = tk.id
    INNER JOIN section s ON tk.course_id = s.course_id
                            AND tk.sec_id = s.sec_id
                            AND tk.semester = s.semester
                            AND tk.year = s.year
    INNER JOIN time_slot ts ON s.time_slot_id = ts.time_slot_id
    INNER JOIN classroom r ON s.building = r.building 
                            AND s.room_number = r.room_number
    INNER JOIN course c ON s.course_id = c.course_id
ORDER BY s.id, s.year, s.semester, ts.day, time_slot
                            
-- 4.2. Student timetable with instructors
WITH day_table AS
    (SELECT 1 AS day_id, 'M' AS day FROM dual
    UNION ALL
    SELECT 2 AS day_id, 'T' AS day FROM dual
    UNION ALL
    SELECT 3 AS day_id, 'W' AS day FROM dual
    UNION ALL
    SELECT 4 AS day_id, 'R' AS day FROM dual
    UNION ALL
    SELECT 5 AS day_id, 'F' AS day FROM dual
    UNION ALL
    SELECT 6 AS day_id, 'S' AS day FROM dual
    UNION ALL
    SELECT 7 AS day_id, 'U' AS day FROM dual)
SELECT s.id AS student_id, s.name AS student_name,
        s.year, s.semester, 
        ts.day, 
        ts.start_hr || ':' || trim(to_char(ts.start_min, '00')) || ' - ' ||
                ts.end_hr || ':' || trim(to_char(ts.end_min, '00')) AS time_slot,
        r.building, r.room_number,
        c.course_id, c.title AS course_title,
        ins.id as lecturer_id, ins.name AS lecturer_name
FROM student s
    INNER JOIN takes tk ON s.id = tk.id
    INNER JOIN section s ON tk.course_id = s.course_id
                            AND tk.sec_id = s.sec_id
                            AND tk.semester = s.semester
                            AND tk.year = s.year
    INNER JOIN time_slot ts ON s.time_slot_id = ts.time_slot_id
    INNER JOIN classroom r ON s.building = r.building 
                            AND s.room_number = r.room_number
    INNER JOIN course c ON s.course_id = c.course_id
    INNER JOIN teaches te ON te.course_id = s.course_id
                            AND te.sec_id = s.sec_id
                            AND te.semester = s.semester
                            AND te.year = s.year
    INNER JOIN instructor ins ON te.id = ins.id
    INNER JOIN day_table ON ts.day = day_table.day
ORDER BY s.id, s.year, s.semester, day_table.day_id, ts.start_hr, ts.start_min

/*
-- Assign one more lecturer into a section
INSERT INTO teaches (ID, year, semester, sec_id, course_id)
VALUES (45565, 2009, 'Fall', 1, 'CS-347')

SELECT LISTAGG(ins.name || ' (' || ins.id || ')', ', ') AS lecturers
FROM instructor ins
*/

-- 4.3. Student timetable with instructors (merge all instructors of one section)
WITH day_table AS
    (SELECT 1 AS day_id, 'M' AS day FROM dual
    UNION ALL
    SELECT 2 AS day_id, 'T' AS day FROM dual
    UNION ALL
    SELECT 3 AS day_id, 'W' AS day FROM dual
    UNION ALL
    SELECT 4 AS day_id, 'R' AS day FROM dual
    UNION ALL
    SELECT 5 AS day_id, 'F' AS day FROM dual
    UNION ALL
    SELECT 6 AS day_id, 'S' AS day FROM dual
    UNION ALL
    SELECT 7 AS day_id, 'U' AS day FROM dual)
SELECT s.id AS student_id, s.name AS student_name,
        s.year, s.semester, 
        ts.day, 
        ts.start_hr || ':' || trim(to_char(ts.start_min, '00')) || ' - ' ||
                ts.end_hr || ':' || trim(to_char(ts.end_min, '00')) AS time_slot,
        r.building, r.room_number,
        c.course_id, c.title AS course_title,
        lecs.lecturers
FROM student s
    INNER JOIN takes tk ON s.id = tk.id
    INNER JOIN section s ON tk.course_id = s.course_id
                            AND tk.sec_id = s.sec_id
                            AND tk.semester = s.semester
                            AND tk.year = s.year
    INNER JOIN time_slot ts ON s.time_slot_id = ts.time_slot_id
    INNER JOIN classroom r ON s.building = r.building 
                            AND s.room_number = r.room_number
    INNER JOIN course c ON s.course_id = c.course_id
    INNER JOIN (SELECT te.course_id, te.sec_id, te.semester, te.year,
                    LISTAGG(ins.name || ' (' || ins.id || ')', ', ') AS lecturers
                FROM teaches te
                    INNER JOIN instructor ins ON te.id = ins.id
                GROUP BY course_id, sec_id, semester, year
                ) lecs
                    ON s.course_id = lecs.course_id
                        AND s.sec_id = lecs.sec_id
                        AND s.semester = lecs.semester
                        AND s.year = lecs.year    
    INNER JOIN day_table ON ts.day = day_table.day
ORDER BY s.id, s.year, s.semester, day_table.day_id, ts.start_hr, ts.start_min
