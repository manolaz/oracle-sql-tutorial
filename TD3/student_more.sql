
-- GET ALL STUDENT YAKE MOST Than COURSE
-- select t.id, t.course_id, count(*) as no_taking
-- from takes t 
-- group by t.id, t.course_id
-- having count(*)>1


-- select *
-- from course c
-- inner join section s on c.course_id = s.course_id
-- inner join time_slot ts ON s.time_slot_id= ts.time_slot_id
-- WHERE c.course_id='BIO-301'

SELECT DISTINCT lec.*
FROM instructor lec,
    teaches te,
    section sec
WHERE
    lec.id = te.id
    AND te.course_id = sec.course_id
    AND te.sec_id = sec.sec_id
    AND te.semester = sec.semester
    AND te.year = sec.year
    AND sec.year = 2009
    

