/*

    Advanced DB - Lab 4

*/

-- 1. Add some more courses

SELECT *
FROM course

INSERT INTO course (course_id, title, dept_name, credits)
VALUES ('CS-102', 'Fundamental of Programming', 'Comp. Sci.', 3)

INSERT INTO course (course_id, title, dept_name, credits)
VALUES ('CS-201', 'Data Struct. and Alg.', 'Comp. Sci.', 4)

INSERT INTO course (course_id, title, dept_name, credits)
VALUES('CS-203', 'Discrete Math.', 'Comp. Sci.', 3)

-- 2. Update the course 'CS-203' to set its credit to 4

-- Check it first
SELECT *
FROM course
WHERE course_id='CS-203'

-- Update
UPDATE course
SET credits=4
WHERE course_id='CS-203'

-- Check it again!
SELECT *
FROM course
WHERE course_id='CS-203'


-- 3. Remove the course 'CS-102'

-- Check it first
SELECT *
-- DELETE
FROM course
WHERE course_id='CS-102'

-- Delete
-- SELECT *
DELETE
FROM course
WHERE course_id='CS-102'

-- 4. Remove all data in the 1) question

-- Check it first
SELECT *
FROM course
WHERE course_id IN ('CS-102', 'CS-203', 'CS-201')

-- Remove them all
--SELECT *
DELETE
FROM course
WHERE course_id IN ('CS-102', 'CS-203', 'CS-201')


---------------------
-- Golden rule for DELETE or UPDATE
--
-- 1. Show the data using SELECT ... FROM ... WHERE conds
--   Check to see if there is any mistakes
--
-- 2. Change the statement into UPDATE/DELETE with the same condition
--   ex: DETELE FROM ... WHERE conds
---------------------


-- 5. Add some courses into the database using INSERT ALL

INSERT ALL 
    INTO course (course_id, title, dept_name, credits)
        VALUES ('CS-102', 'Fundamental of Programming', 'Comp. Sci.', 3)
    INTO course (course_id, title, dept_name, credits)
        VALUES ('CS-201', 'Data Struct. and Alg.', 'Comp. Sci.', 4)
    INTO course (course_id, title, dept_name, credits)
        VALUES ('CS-203', 'Discrete Math.', 'Comp. Sci.', 3)
    INTO student (id, name)
        VALUES (9999, 'XXX')
    SELECT * FROM dual
    
SELECT *
FROM student

-- (*) 6. Migrate the course table!

SELECT 'INSERT INTO course(course_id, title, dept_name, credits) VALUES (''' 
        || course_id || ''', ''' 
        || title || ''', ''' 
        || dept_name || ''', '
        || credits 
        || ');'
        AS txt
FROM course

SELECT 'INSERT ALL' AS txt 
FROM dual
UNION ALL
SELECT 'INTO course(course_id, title, dept_name, credits) VALUES (''' 
        || course_id || ''', ''' 
        || title || ''', ''' 
        || dept_name || ''', '
        || credits 
        || ')'
        AS txt
FROM course

-- Into another table
CREATE TABLE course1
AS
SELECT *
FROM course
WHERE 1=0

SELECT *
FROM course1

INSERT INTO course1 (course_id, title, dept_name, credits)
SELECT course_id, title, dept_name, credits
FROM course
WHERE course_id IN ('CS-102', 'CS-203', 'CS-201')

DELETE 
FROM course1

INSERT INTO course1 (course_id, title, dept_name, credits)
SELECT course_id, title, dept_name, credits
FROM ( 
        SELECT 'CS-102' AS course_id, 'Fundamental of Programming' AS title, 'Comp. Sci.' AS dept_name, 3 AS credits
        FROM dual
        UNION ALL
        SELECT 'CS-201' AS course_id, 'Data Struct. and Alg.' AS title, 'Comp. Sci.' AS dept_name, 4 AS credits
        FROM dual
        UNION ALL
        SELECT 'CS-203' AS course_id, 'Discrete Math.' AS title, 'Comp. Sci.' AS dept_name, 3 AS credits
        FROM dual
    ) tmp


-- 7. Add more prerequisites to a course

---- Pass the 'Intro' course before taking 'programming' course

---- Pass the 'programming' course before taking 'image processing' course

SELECT *
FROM course

INSERT ALL 
    INTO prereq (course_id, prereq_id)
    VALUES('CS-101', 'CS-102')
    INTO prereq (course_id, prereq_id)
    VALUES('CS-102', 'CS-102')
SELECT * FROM dual


--- Check if any student violates the prerequisites when taking a course?
SELECT *
FROM takes pre_t
    INNER JOIN course pre_c ON pre_t.course_id = pre_c.course_id
    INNER JOIN prereq pre ON pre_c.course_id = pre.prereq_id
    INNER JOIN course post_c ON pre.course_id = post_c.course_id
    INNER JOIN takes post_t ON post_c.course_id = post_t.course_id
WHERE pre_t.grade='F'
        AND pre_t.id = post_t.id
        AND (pre_t.year < post_t.year 
            OR (pre_t.year = post_t.year 
                AND ((pre_t.semester='Spring' AND post_t.semester='Summer')
                    OR (pre_t.semester='Spring' AND post_t.semester='Fall')
                    OR (pre_t.semester='Summer' AND post_t.semester='Fall')
                    )
                )
            )
        

-- 8. Make sure that, the prerequisites of 'CS-347' is the same of 'CS-319'

-- Collect prerequisites of 'CS-319'
SELECT *
FROM prereq
WHERE course_id = 'CS-319'

-- Collect prerequisites of 'CS-347'
SELECT *
FROM prereq
WHERE course_id = 'CS-347'

-- Fake data one for CS-319 and one for CS-347
INSERT ALL 
    INTO prereq (course_id, prereq_id)
    VALUES('CS-319', 'CS-102')
    INTO prereq (course_id, prereq_id)
    VALUES('CS-347', 'CS-201')
SELECT * FROM dual

-- Simple way: remove all prereqs of CS-347 and them import all prereqs from CS-319
-- "Stupid" way: (1) remove prereqs of CS-347 those are not in that of CS-319 
--           and (2) import from prereqs of CS-319 for those are not in that of CS-347
-- "Safety" way: (1) import from prereqs of CS-319 for those are not in that of CS-347
--           and (2) remove prereqs of CS-347 those are not in that of CS-319 


INSERT INTO prereq (course_id, prereq_id)
-- Collect prerequisites of 'CS-319' not in 'CS-347'
SELECT 'CS-347' AS course_id, prereq_id
FROM prereq
WHERE course_id = 'CS-319'
MINUS
SELECT 'CS-347' AS course_id, prereq_id
FROM prereq
WHERE course_id = 'CS-347'

-- SELECT prereq_id
DELETE
FROM prereq
WHERE prereq_id NOT IN
        (SELECT prereq_id
        FROM prereq
        WHERE course_id = 'CS-319')
    AND course_id = 'CS-347'

-- 9. Update the total credits earned by a student
---- Assume that, at least 'C' to pass a course
---- course is counted once

-- Courses that a student passed
-- A course is counted only one
SELECT DISTINCT c.course_id, c.credits
FROM takes t
    INNER JOIN course c ON t.course_id = c.course_id
WHERE t.grade <= 'C-'
    AND t.id = '00128'

-- Check if C+ <= C- :)
SELECT 1
FROM dual
WHERE 'C+' <= 'C-'

-- Credites that a student earned
SELECT SUM(a.credits) AS tot_cred
FROM (
    SELECT DISTINCT c.course_id, c.credits
    FROM takes t
        INNER JOIN course c ON t.course_id = c.course_id
    WHERE t.grade <= 'C-'
        AND t.id = '00128'
    ) a

-- Update for all students
UPDATE student
SET tot_cred = (
                SELECT SUM(a.credits) AS tot_cred
                FROM (
                    SELECT DISTINCT c.course_id, c.credits
                    FROM takes t
                        INNER JOIN course c ON t.course_id = c.course_id
                    WHERE t.grade <= 'C-'
                        AND t.id = student.id
                    ) a
                )

-- 10. Update the "total courses" and "total credits" earned by a student
---- Assume that, at least 'C' to pass a course
---- course is counted once