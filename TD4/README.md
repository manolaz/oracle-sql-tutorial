# TD4 - Thu 06-Aug-2020

/*

    Advanced DB - Lab 3

*/

-- 1. Add some more courses

'CS-102', 'Fundamental of Programming', 'Comp. Sci.', 3

'CS-201', 'Data Struct. and Alg.', 'Comp. Sci.', 4

'CS-203', 'Discrete Math.', 'Comp. Sci.'



-- 2. Update the course 'CS-203' to set its credit to 4

-- 3. Remove the course 'CS-102'

-- 4. Remove all data in the 1) question

-- 5. Add some courses into the database using INSERT ALL

'CS-102', 'Fundamental of Programming', 'Comp. Sci.', 3

'CS-201', 'Data Struct. and Alg.', 'Comp. Sci.', 4

'CS-203', 'Discrete Math.', 'Comp. Sci.'

-- (*) 6. Migrate the course table!

-- 7. Add more prerequisites to a course

---- Pass the 'Intro' course before taking 'programming' course

---- Pass the 'programming' course before taking 'image processing' course

-- 8. Make sure that, the prerequisites of 'CS-347' is the same of 'CS-319'

-- 9. Update the total credits earned by a student

---- Assume that, at least 'C' to pass a course

---- course is counted once

-- 10. Update the "total courses" and "total credits" earned by a student

---- Assume that, at least 'C' to pass a course

---- course is counted once