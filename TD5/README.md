# TD5 - Tue 11-Aug-2020

/*

    Advanced DB - Lab 5

*/

--------------------------

--- Part A. Execution plan

--------------------------

-- 1. Explain how a sql statement is executed

-- 2. Show instructors who have classes,

-- then compare the performance of running several sql statement solutions

--------------------------

--- Part B. Database schema

--------------------------

-- 1. Database schema for program curriculum

---- 1.1. major

---- 1.2. program

---- 1.3. program part

---- 1.4. program part consists of some courses

-- 2. Upgrade the schema

-- 3. Show the curriculumn

-- 4. Auto increase primary key

-- 5. Create a list of employees and their department name for department 10 or 20

-- 6. Can we insert into a view?