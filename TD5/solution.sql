/*

    Advanced DB - Lab 5

*/

--------------------------

--- Part A. Execution plan

--------------------------

create table PLAN_TABLE ( 
        statement_id       varchar2(30), 
        plan_id            number, 
        timestamp          date, 
        remarks            varchar2(4000), 
        operation          varchar2(30), 
        options            varchar2(255), 
        object_node        varchar2(128), 
        object_owner       varchar2(30), 
        object_name        varchar2(30), 
        object_alias       varchar2(65), 
        object_instance    numeric, 
        object_type        varchar2(30), 
        optimizer          varchar2(255), 
        search_columns     number, 
        id                 numeric, 
        parent_id          numeric, 
        depth              numeric, 
        position           numeric, 
        cost               numeric, 
        cardinality        numeric, 
        bytes              numeric, 
        other_tag          varchar2(255), 
        partition_start    varchar2(255), 
        partition_stop     varchar2(255), 
        partition_id       numeric, 
        other              long, 
        distribution       varchar2(30), 
        cpu_cost           numeric, 
        io_cost            numeric, 
        temp_space         numeric, 
        access_predicates  varchar2(4000), 
        filter_predicates  varchar2(4000), 
        projection         varchar2(4000), 
        time               numeric, 
        qblock_name        varchar2(30), 
        other_xml          clob 
)


-- 1. Explain how a sql statement is executed
EXPLAIN PLAN
SET statement_id='query1'
FOR
SELECT empno, ename, job, hiredate, deptno
FROM scott.emp
WHERE EXTRACT(YEAR FROM hiredate) = 1987
ORDER BY sal

SELECT LPAD('............................',2*(LEVEL-1))||operation operation, options,   
object_name, position, cardinality, cost   
    FROM plan_table   
    START WITH id = 0 AND statement_id = 'query1'  
    CONNECT BY PRIOR id = parent_id AND statement_id = 'query1'

DELETE FROM plan_table
WHERE statement_id = 'query2'

EXPLAIN PLAN
SET statement_id='query2'
FOR
SELECT empno, ename, job, hiredate, deptno
FROM scott.emp
WHERE empno=20
ORDER BY sal

SELECT LPAD('............................',2*(LEVEL-1))||operation operation, options,   
object_name, position, cardinality, cost  
    FROM plan_table   
    START WITH id = 0 AND statement_id = 'query2'  
    CONNECT BY PRIOR id = parent_id AND statement_id = 'query2'


-- 2. Show instructors who have classes, 

-- then compare the performance of running several sql statement solutions


EXPLAIN PLAN   
    SET STATEMENT_ID = 'firstq1'  
    FOR  
SELECT id, name AS instructor_name
FROM instructor
WHERE id*3 IN (SELECT id*3 
            FROM teaches)

EXPLAIN PLAN   
    SET STATEMENT_ID = 'secondq'  
    FOR  
SELECT DISTINCT id, name AS instructor_name
FROM instructor
    NATURAL JOIN teaches

EXPLAIN PLAN   
    SET STATEMENT_ID = 'thirdq'  
    FOR  
SELECT id, name AS instructor_name
FROM instructor
WHERE EXISTS (SELECT 1
            FROM teaches
            WHERE teaches.id = instructor.id)


SELECT 'firstq' as query, LPAD('...........................',2*(LEVEL-1))||operation operation, options,   
object_name, position, cost, cardinality   
    FROM plan_table   
    START WITH id = 0 AND statement_id = 'firstq1'  
    CONNECT BY PRIOR id = parent_id AND statement_id = 'firstq1'
UNION
SELECT  'secondq' as query, LPAD('............................',2*(LEVEL-1))||operation operation, options,   
object_name, position, cost, cardinality   
    FROM plan_table   
    START WITH id = 0 AND statement_id = 'secondq'  
    CONNECT BY PRIOR id = parent_id AND statement_id = 'secondq'
UNION
SELECT  'thirdq' as query, LPAD('............................',2*(LEVEL-1))||operation operation, options,   
object_name, position, cost, cardinality   
    FROM plan_table   
    START WITH id = 0 AND statement_id = 'thirdq'  
    CONNECT BY PRIOR id = parent_id AND statement_id = 'thirdq'
order by 1, 2 desc

--------------------------

--- Part B. Database schema

--------------------------

-- 1. Database schema for program curriculum

---- 1.1. major
CREATE TABLE major (
    major_id VARCHAR2(20),
    major_name VARCHAR2(100) NOT NULL,
    dept_name VARCHAR2(20) NOT NULL,
    PRIMARY KEY (major_id)
)

---- populate the major table
INSERT INTO major(major_id, major_name, dept_name)
VALUES ('CS', 'Computer Science', 'Comp. Sci.')

INSERT INTO major(major_id, major_name, dept_name)
VALUES ('IT', 'Information Technology', 'Comp. Sci.')

SELECT *
FROM major


---- 1.2. program
CREATE TABLE program (
    prog_id VARCHAR2(20),
    prog_name VARCHAR2(100),
    intake INT,
    major_id VARCHAR2(20),
    PRIMARY KEY (prog_id),
    FOREIGN KEY (major_id) REFERENCES major(major_id)
)

---- populate the program table
INSERT INTO program(prog_id, prog_name, intake, major_id)
VALUES ('CS-19', 'Computer Science - intake 19', 19, 'CS')

INSERT INTO program(prog_id, prog_name, intake, major_id)
VALUES ('IT-19', 'Information Technology - intake 19', 19, 'IT')

SELECT *
FROM program

---- 1.3. program part
CREATE TABLE program_part (
    progpart_id VARCHAR2(20),
    progpart_name VARCHAR2(100),
    elective INT,
    credits INT,
    prog_id VARCHAR2(20),
    PRIMARY KEY (progpart_id),
    FOREIGN KEY (prog_id) REFERENCES program(prog_id)
)

---- populate the program_part table
INSERT INTO program_part(progpart_id, progpart_name, elective, credits, prog_id)
VALUES ('CS-19-MATH', 'Computer Science - Maths', 0, 8, 'CS-19')

INSERT INTO program_part(progpart_id, progpart_name, elective, credits, prog_id)
VALUES ('CS-19-PROG', 'Computer Science - Programming', 0, 12, 'CS-19')

INSERT INTO program_part(progpart_id, progpart_name, elective, credits, prog_id)
VALUES ('CS-19-ECO', 'Computer Science - Economics', 1, 4, 'CS-19')


SELECT *
FROM program_part


---- 1.4. program part consists of some courses

CREATE TABLE program_part_course (
    progpart_id VARCHAR2(20),
    course_id VARCHAR2(20),
    PRIMARY KEY (progpart_id, course_id),
    CONSTRAINT progpart_id_fk FOREIGN KEY (progpart_id) REFERENCES program_part(progpart_id),
    FOREIGN KEY (course_id) REFERENCES course(course_id)
)

---- populate the program_part_course table
INSERT INTO program_part_course(progpart_id, course_id)
VALUES ('CS-19-MATH', 'CS-101');

INSERT INTO program_part_course(progpart_id, course_id)
VALUES ('CS-19-PROG', 'CS-190');

INSERT INTO program_part_course(progpart_id, course_id)
VALUES ('CS-19-PROG', 'CS-347');


SELECT *
FROM program_part_course

-- 2. Upgrade the schema

---- NOT NULL constraint
ALTER TABLE major
MODIFY (major_name VARCHAR2(100) NOT NULL,
        dept_name VARCHAR2(20) NOT NULL
        )


ALTER TABLE program_part_course
DROP CONSTRAINT SYS_C0035387050

ALTER TABLE program_part_course
DROP CONSTRAINT SYS_C0035387051

ALTER TABLE program_part_course
ADD (CONSTRAINT programpart_id_FK FOREIGN KEY (progpart_id) REFERENCES program_part(progpart_id),
    CONSTRAINT course_id_FK FOREIGN KEY (course_id) REFERENCES course(course_id)
    )

SELECT a.constraint_name, a.constraint_type, a.search_condition, a.table_name, a.status, a.r_constraint_name,
    b.table_name AS r_table_name
FROM all_constraints a
    LEFT JOIN all_constraints b ON a.r_constraint_name = b.constraint_name
WHERE a.table_name = 'PROGRAM_PART'

---- make sure that the "elective" is 0 (means compulsory) or 1 (means elective)
ALTER TABLE program_part
ADD (CHECK (elective=0 OR elective=1))


-- 3. Show the curriculumn
SELECT prg.*, mjr.major_name, 
    prgp.progpart_name, 
    (CASE WHEN prgp.elective=1 THEN 'Y' ELSE 'N' END) AS "elective?", 
    prgp.credits,
    c.course_id, c.title, c.credits
FROM program prg
    INNER JOIN major mjr ON prg.major_id = mjr.major_id
    INNER JOIN program_part prgp ON prgp.prog_id = prg.prog_id
    LEFT JOIN program_part_course prgpc ON prgpc.progpart_id = prgp.progpart_id
    LEFT JOIN course c ON prgpc.course_id = c.course_id
WHERE prg.intake = 19 AND prg.major_id='CS'
ORDER BY prg.prog_id, prgp.progpart_id


-- 4. Auto increase primary key
CREATE TABLE table1 (
    id NUMBER GENERATED AS IDENTITY PRIMARY KEY,
    name VARCHAR2(100)
    )

INSERT INTO table1 (name) VALUES ('Case 1')
INSERT INTO table1 (name) VALUES ('Case 2')

SELECT *
FROM table1

-- Wrong! 
-- INSERT INTO table1 (id, name) VALUES (3, 'Case 3')

CREATE SEQUENCE myseq
    START WITH 1
    INCREMENT BY 1;

SELECT myseq.NEXTVAL 
FROM dual

CREATE TABLE table2 (
    id NUMBER DEFAULT myseq.NEXTVAL PRIMARY KEY,
    name VARCHAR2(100)
    )
    
INSERT INTO table2 (name) VALUES ('Case 1')
INSERT INTO table2 (id, name) VALUES (10, 'Case 2')
INSERT INTO table2 (name) VALUES ('Case 3')

SELECT * FROM table2

-- 5. Create a list of employees and their department name for department 10 or 20

CREATE VIEW emp10_20 
AS
SELECT e.empno, e.ename, e.job, e.mgr, e.hiredate, e.sal, e.comm, e.deptno, d.dname
FROM scott.emp e
    INNER JOIN scott.dept d ON e.deptno = d.deptno
WHERE e.deptno IN (10, 20)

SELECT *
FROM emp10_20

-- 6. Can we insert into a view?

INSERT INTO emp10_20 (empno, ename, job, mgr, hiredate, sal, comm, deptno, dname)
VALUES (9999, 'XXX', 'CLERK', NULL, SYSDATE, 9000, NULL, 10, 'ACCOUNTING')

-- Simple view
CREATE OR REPLACE VIEW studentCS
AS
SELECT id, name, dept_name, tot_cred
FROM student
WHERE dept_name='Comp. Sci.'
WITH CHECK OPTION


SELECT *
FROM studentCS

INSERT INTO studentCS (id, name, dept_name, tot_cred)
VALUES ('55555', 'New student', 'Comp. Sci.', 50)

-- Violate the rule
INSERT INTO studentCS (id, name, dept_name, tot_cred)
VALUES ('00000', 'New student 1', 'Physics', 50)