/*

    Advanced DB - Lab 6

*/

-- 1. A simple function
CREATE OR REPLACE FUNCTION inc(x INTEGER)
RETURN INTEGER
AS
BEGIN
    RETURN (x+1);
END;

---- expected: 11
SELECT inc(10)
FROM dual

---- 
SELECT s.*, inc(s.tot_cred)
FROM student s

-- 2. Get the week of a date
SELECT TO_CHAR(SYSDATE, 'IW') AS week
FROM dual

CREATE OR REPLACE FUNCTION weekOfDate(d DATE)
RETURN INTEGER
AS
BEGIN
    RETURN TO_NUMBER(TO_CHAR(d, 'IW'));
END;

SELECT weekOfDate(SYSDATE)
FROM dual

SELECT e.*, weekOfDate(e.hiredate)
FROM scott.emp e

-- 3. Get the date from a day of a week
---- a date before the first date of the week
SELECT NEXT_DAY(TRUNC(TO_DATE('01-01-2020', 'DD-MM-YYYY')+(33-1)*7, 'IW')-1,'THU') AS mydate
FROM dual

SELECT NEXT_DAY(SYSDATE,'THU') AS mydate
FROM dual

---- the function
CREATE OR REPLACE FUNCTION dateOfWeek(y INTEGER, w INTEGER, dy VARCHAR2)
RETURN DATE
AS
BEGIN
    RETURN NEXT_DAY(TRUNC(TO_DATE('01-01-' || y, 'DD-MM-YYYY')+(w-1)*7,'IW')-1, dy);
END;

SELECT dateOfWeek(2020, 33, 'MON'), dateOfWeek(2020, 33, 'SAT'), dateOfWeek(2020, 33, 'SUN')
FROM dual


-- 4. Make a long string of a repeated character
CREATE OR REPLACE FUNCTION repeatString (s VARCHAR2, len INTEGER)
RETURN VARCHAR2
AS
    i INTEGER := 1;
    ret VARCHAR2(1000) := '';
BEGIN
    FOR i IN 1..len 
    LOOP
        ret := ret || s;
    END LOOP;
    RETURN ret;
END;

SELECT repeatString('abc', 3)
FROM dual

CREATE OR REPLACE FUNCTION repeatString (s VARCHAR2, len INTEGER)
RETURN VARCHAR2
AS
    i INTEGER := 1;
    ret VARCHAR2(1000) := '';
BEGIN
    WHILE i<=len 
    LOOP
        ret := ret || s;
        i := i+1;
    END LOOP;
    RETURN ret;
END;

CREATE OR REPLACE FUNCTION repeatString (s VARCHAR2, len INTEGER)
RETURN VARCHAR2
AS
    i INTEGER := 1;
    ret VARCHAR2(1000) := '';
BEGIN
    LOOP
        ret := ret || s;
        i := i+1;

        EXIT WHEN i>len;
    END LOOP;
    RETURN ret;
END;

-- 5. An anonymous PL/SQL block
DECLARE
    cond INTEGER;
BEGIN
    cond := 2;
    
    IF (cond > 3) THEN
        DBMS_OUTPUT.PUT_LINE('>3');
    ELSIF (cond > 1) THEN
        DBMS_OUTPUT.PUT_LINE('>1 and <=3');
    ELSE
        DBMS_OUTPUT.PUT_LINE('<=1');
    END IF;
END;

DECLARE
    cond INTEGER;
BEGIN
    cond := 2;
    
    CASE cond
        WHEN 3 THEN
            DBMS_OUTPUT.PUT_LINE('3');
        WHEN 1 THEN
            DBMS_OUTPUT.PUT_LINE('1');
        ELSE
            DBMS_OUTPUT.PUT_LINE('not 1 nor 3');
    END CASE;
END;

SELECT s.*, (CASE WHEN (s.tot_cred > 100) THEN 'Good!' ELSE 'Try more!' END) AS ccc
FROM student s

-- 6. Update the total credits for all students
CREATE OR REPLACE PROCEDURE updateStudentCredit
AS
BEGIN
    UPDATE student
    SET tot_cred = (
            SELECT SUM(earn.credits) AS total_credits
            FROM (
                SELECT DISTINCT t.id, t.course_id, c.credits
                FROM takes t
                    INNER JOIN course c ON t.course_id = c.course_id
                WHERE t.grade <= 'C-'
                    AND t.id = student.id
                ) earn
        );
END;

SELECT *
FROM student

EXEC updateStudentCredit

-- 7. Update the total credits for a student
CREATE OR REPLACE PROCEDURE updateOneStudentCredit (stu_id VARCHAR2)
AS
BEGIN
    UPDATE student
    SET tot_cred = (
            SELECT SUM(earn.credits) AS total_credits
            FROM (
                SELECT DISTINCT t.id, t.course_id, c.credits
                FROM takes t
                    INNER JOIN course c ON t.course_id = c.course_id
                WHERE t.grade <= 'C-'
                    AND t.id = student.id
                ) earn
        )
    WHERE id = stu_id;
END;

UPDATE student
SET tot_cred = 0
WHERE id='12345'

EXEC updateOneStudentCredit('12345')

-- Error: wrong number or types of arguments in call to
EXEC updateOneStudentCredit()
EXEC updateOneStudentCredit(123, 456)

SELECT *
FROM student
WHERE id='12345'

-- 8. Update the total credits for a student/all students
CREATE OR REPLACE PROCEDURE updateSomeStudentCredit (stu_id VARCHAR2 DEFAULT NULL)
AS
BEGIN
    UPDATE student
    SET tot_cred = (
            SELECT SUM(earn.credits) AS total_credits
            FROM (
                SELECT DISTINCT t.id, t.course_id, c.credits
                FROM takes t
                    INNER JOIN course c ON t.course_id = c.course_id
                WHERE t.grade <= 'C-'
                    AND t.id = student.id
                ) earn
        )
    WHERE (id = stu_id) OR (stu_id IS NULL);
END;

UPDATE student
SET tot_cred = 0

---- Omit the para -> NULL value will be sent
EXEC updateSomeStudentCredit()

SELECT *
FROM student

-- 9. Extract the first name in Vietnamese full name
-- By using SUBSTR, INSTR, REVERSE functions
SELECT REVERSE(SUBSTR(REVERSE('Bui Hoai Thang'), 1, INSTR(REVERSE('Bui Hoai Thang'), ' ')))
FROM dual

-- A function
CREATE OR REPLACE FUNCTION getFirstVNName(s VARCHAR2)
RETURN VARCHAR2
AS
    s1 VARCHAR2(1000) := s;
    ind INTEGER;
BEGIN
    s1 := TRIM(s);      -- Remove all leading/tailing blank characters
    ind := LENGTH(s1); -- From the last to the beginning
    
    LOOP
        -- Stop when reaching (over) the first character
        -- or we have found the last blank character of the name
        EXIT WHEN ((ind = 0) OR (SUBSTR(s1, ind, 1) = ' '));
        ind := ind-1; 
    END LOOP;
    
    -- So, we are that the last blank character
    -- or at index 0, 
    -- then get the substring from the index+1 untill the end of the string
    RETURN SUBSTR(s1, ind+1, LENGTH(s1));
END;

SELECT getFirstVNName('Bui Hoai Thang')
FROM dual

SELECT getFirstVNName('K''Sor')
FROM dual

-- 9. Show the list of student
DECLARE 
    CURSOR c_stu IS
        SELECT id, name
        FROM student;
        
    stu_id VARCHAR2(100);
    stu_name VARCHAR2(100);
BEGIN
    OPEN c_stu;
    LOOP
        FETCH c_stu INTO stu_id, stu_name;
        EXIT WHEN c_stu%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(stu_id || ' - ' || stu_name);
    END LOOP;
    CLOSE c_stu;
END;

---- fetch into a structure
DECLARE 
    CURSOR c_stu IS
        SELECT id, name AS stu_name
        FROM student;
        
    stu c_stu%ROWTYPE; -- stu is a structure of c_stu (id, name)
BEGIN
    OPEN c_stu;
    WHILE true LOOP
        FETCH c_stu INTO stu;
        EXIT WHEN c_stu%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(stu.id || ' - ' || stu.stu_name);
    END LOOP;
    CLOSE c_stu;
END;

---- fetch into a structure
DECLARE 
    CURSOR c_stu IS
        SELECT *
        FROM student;
        
    stu c_stu%ROWTYPE; -- stu is a structure of c_stu ~ student table (id, name, ...)
BEGIN
    OPEN c_stu;
    WHILE true LOOP
        FETCH c_stu INTO stu;
        EXIT WHEN c_stu%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(stu.id || ' - ' || stu.name || ' - ' || stu.tot_cred);
    END LOOP;
    CLOSE c_stu;
END;

/*
---- fetch one student into a structure
DECLARE 
    stu student%ROWTYPE;
BEGIN
    SELECT * 
    INTO stu
    FROM student
    WHERE id='12345';
    
    DBMS_OUTPUT.PUT_LINE(stu.id || ' - ' || stu.name || ' - ' || stu.tot_cred);
END;
*/

---- for loop with cursor
DECLARE 
    CURSOR c_stu IS
        SELECT id, name
        FROM student;
BEGIN
    FOR stu IN c_stu 
    LOOP
        DBMS_OUTPUT.PUT_LINE(stu.id || ' - ' || stu.name);
    END LOOP;
END;

---- for loop with implicit cursor
BEGIN
    FOR stu IN 
        (SELECT id, name
        FROM student)
    LOOP
        DBMS_OUTPUT.PUT_LINE(stu.id || ' - ' || stu.name);
    END LOOP;
END;

-- 10. Show the list of student of a department
DECLARE 
    CURSOR c_stu (dept VARCHAR2) IS
        SELECT id, name
        FROM student
        WHERE dept_name=dept;
BEGIN
    FOR stu IN c_stu ('Comp. Sci.') LOOP
        DBMS_OUTPUT.PUT_LINE(stu.id || ' - ' || stu.name);
    END LOOP;
END;


CREATE OR REPLACE PROCEDURE listOfStudentDept (dept VARCHAR2)
IS
    CURSOR c_stu (v_dept VARCHAR2) IS
        SELECT id, name
        FROM student
        WHERE dept_name=v_dept;
BEGIN
    FOR stu IN c_stu (dept) LOOP
        DBMS_OUTPUT.PUT_LINE(stu.id || ' - ' || stu.name);
    END LOOP;
END;

EXEC listOfStudentDept ('Physics')

-- 11. Get the list of registered student for a course, given the name of the course
CREATE OR REPLACE PROCEDURE listOfStudentCourse (c_title VARCHAR2)
IS
    CURSOR c_stu IS
        SELECT id, name
        FROM student
        WHERE id IN (SELECT id 
                    FROM takes t
                        INNER JOIN course c ON t.course_id = c.course_id
                    WHERE c.title = c_title);
BEGIN
    FOR stu IN c_stu LOOP
        DBMS_OUTPUT.PUT_LINE(stu.id || ' - ' || stu.name);
    END LOOP;
END;

SELECT *
FROM course

EXEC listOfStudentCourse ('Intro. to Biology')


CREATE OR REPLACE FUNCTION studentsOfCourse(c_title VARCHAR2)
RETURN VARCHAR2
AS
    v_list VARCHAR2(1000) := 'no one';
    v_course_id course.course_id%TYPE;
BEGIN
    -- Get the course_id
    -- may cause not found
    SELECT course_id
    INTO v_course_id
    FROM course
    WHERE title = c_title;
    
    SElECT LISTAGG(s.id || ' - ' || s.name, ', ') 
    INTO v_list
    FROM takes t
        INNER JOIN student s ON t.id = s.id
    WHERE course_id = v_course_id;
    
    RETURN v_list;

EXCEPTION
    -- catch the exception
    WHEN NO_DATA_FOUND THEN -- no data for the SELECT INTO
        RETURN 'no one!';
END;

SELECT studentsOfCourse('Intro. to Computer Science')
FROM dual

-- Error -> catch exeption -> return 'no one!'
SELECT studentsOfCourse('Intro. to Computer')
FROM dual

-- 12. Calculate the total credits for a student

-- return -1 for any error

-- 13. Calculate the number of 'earned' courses for a student

-- return -1 for any error