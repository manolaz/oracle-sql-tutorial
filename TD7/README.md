TD7 - Mon 24-Aug-2020
/*

    Advanced DB - Lab 7

*/

-- 1. Check prerequisite when a student want to enroll a course

---- (the student has to pass all prerequisite courses of the course)

/*

    This function check if a student passed all prerequite courses
    of a given course. This function will be used before allowing 
    the student enroll into a course.    
    @param   p_student_id  The student we want to check
    @param   p_course_id  The course the student want to enroll
    @return INTEGER
        1   passed all prerequites
        0   failed some prerequisites
*/

-- 2. Check prerequisite when a student want to enroll a course
---- and return the list of failed prerequisites, if any
---- (the student has to pass all prerequisite courses of the course)

/*

    This function check if a student passed all prerequisite courses

    of a given course or return a list of failed prerequisites.

    This function will be used before allowing the student enroll into a course.

    @param   p_student_id  The student we want to check
    @param   p_course_id  The course the student want to enroll
    @return VARCHAR2
        ''   passed all prerequites
        <>''   failed prerequisites
*/

-- 3. Make sure that when a student enroll into a course, he/she must have passed all prerequisites

-- 4. Make the history for changing department of instructors

---- Create a table to store the history

---- Capture the changing

---- Test