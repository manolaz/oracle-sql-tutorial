/*
    Advanced DB - Lab 7
*/

-- 1. Check prerequisite when a student want to enroll a course
---- (the student has to pass all prerequisite courses of the course)

SELECT *
FROM prereq
WHERE prereq_id='CS-101'


SELECT count(*)
FROM takes
WHERE course_id = 'CS-101'
    AND grade <= 'C+'
    AND id = '345678'


SELECT *
FROM takes
WHERE id='45678'

DELETE FROM takes
WHERE id='45678' AND course_id='CS-101'


SELECT prereq_id
FROM prereq
WHERE course_id = 'CS-319'


SELECT *
FROM takes t
    RIGHT JOIN 
        (SELECT * 
        FROM prereq pr
        WHERE pr.course_id = 'CS-101') prc
        ON t.course_id = prc.prereq_id
AND t.grade <= 'C+'
    AND t.id = '45678'

SELECT pre.course_id, pre.prereq_id, count(*)
FROM prereq pre
    LEFT JOIN takes t
        ON pre.prereq_id = t.course_id
            AND t.grade <= 'C+'
            AND t.id = '345678'
WHERE
    pre.course_id = 'CS-333'
--    AND t.id = '345678'
GROUP BY pre.course_id, pre.prereq_id


/*
    This function check if a student passed all prerequite courses
    of a given course. This function will be used before allowing 
    the student enroll into a course.
    
    @param   p_student_id  The student we want to check
    @param   p_course_id  The course the student want to enroll
    
    @return INTEGER
        1   passed all prerequites
        0   failed some prerequisites
*/
CREATE OR REPLACE FUNCTION check_prerequisite(p_student_id VARCHAR2, p_course_id VARCHAR2)
RETURN INT
AS
    v_notpassed INTEGER;
    v_no_prereqs INTEGER;
BEGIN
    SELECT count(*)
    INTO v_no_prereqs
    FROM prereq
    WHERE course_id = p_course_id;

    IF (v_no_prereqs > 0) THEN
        SELECT COUNT(*)
        INTO v_notpassed
        FROM prereq pre
            LEFT JOIN takes t
                ON pre.prereq_id = t.course_id
                    AND t.grade <= 'C+'
                    AND t.id = p_student_id
        WHERE
            pre.course_id = p_course_id
            AND t.id is null;
    
        IF (v_notpassed > 0) THEN -- There are some prerequisite courses that the student has not passed yet
            RETURN 0;
        ELSE
            RETURN 1;
        END IF;
    ELSE
        RETURN 1;
    END IF;
END;

SELECT st.*, check_prerequisite(st.id, 'CS-347') AS can_take_CS_347
FROM student st

-- 2. Check prerequisite when a student want to enroll a course
---- and return the list of failed prerequisites, if any
---- (the student has to pass all prerequisite courses of the course)
/*
    This function check if a student passed all prerequite courses
    of a given course or return a list of failed prerequisites.
    This function will be used before allowing the student enroll into a course.
    
    @param   p_student_id  The student we want to check
    @param   p_course_id  The course the student want to enroll
    
    @return VARCHAR2
        ''   passed all prerequites
        <>''   failed prerequisites
*/
CREATE OR REPLACE FUNCTION get_prerequisite(p_student_id VARCHAR2, p_course_id VARCHAR2)
RETURN VARCHAR2
AS
    v_notpassed VARCHAR2(200);
    v_no_prereqs INTEGER;
BEGIN
    SELECT count(*)
    INTO v_no_prereqs
    FROM prereq
    WHERE course_id = p_course_id;

    IF (v_no_prereqs > 0) THEN

        SELECT LISTAGG(pre.prereq_id)
        INTO v_notpassed
        FROM prereq pre
            LEFT JOIN takes t
                ON pre.prereq_id = t.course_id
                    AND t.grade <= 'C+'
                    AND t.id = p_student_id
        WHERE
            pre.course_id = p_course_id
            AND t.id is null;
        
        RETURN v_notpassed;
    ELSE
        RETURN '';
    END IF;
END;

SELECT st.*, get_prerequisite(st.id, 'CS-347') AS need_2_take_CS_347
FROM student st


-- 3. Make sure that when a student enroll into a course, he/she must have passed all prerequisites
CREATE OR REPLACE TRIGGER takes_check_prereq_trig
BEFORE INSERT OR UPDATE
ON takes
FOR EACH ROW
BEGIN
    IF (check_prerequisite(:NEW.id, :NEW.course_id)=0) THEN
        RAISE_APPLICATION_ERROR(-20000, 'Violate the prerequisites');
    END IF;
END;

SELECT check_prerequisite('45678', 'CS-347')
FROM dual

INSERT INTO takes (id, course_id, sec_id, semester, year)
VALUES ('45678', 'CS-347', 1, 'Fall', 2009)

-- 4. Make the history for changing department of instructors
---- Create a table to store the history
CREATE TABLE dept_history (
    instructor_id   VARCHAR2(20),
    dept_name       VARCHAR2(20),
    changed_date    DATE,
    PRIMARY KEY (instructor_id, changed_date),
    FOREIGN KEY (instructor_id) REFERENCES instructor(id),
    FOREIGN KEY (dept_name) REFERENCES department(dept_name)
)

---- Capture the changing
CREATE OR REPLACE TRIGGER inst_change_dept_trig
AFTER UPDATE OF dept_name 
ON instructor
FOR EACH ROW
BEGIN
    IF (:NEW.dept_name <> :OLD.dept_name) THEN
        INSERT INTO dept_history (instructor_id, dept_name, changed_date)
        VALUES (:OLD.id, :OLD.dept_name, SYSDATE);
    END IF;
END;

---- Test
SELECT *
FROM instructor

SELECT dh.*, TO_CHAR(dh.changed_date, 'DD-MON-YYYY HH:MI:SS') as long_date
FROM dept_history dh

UPDATE instructor
SET dept_name = 'Physics'
WHERE id='10101'


UPDATE instructor
SET salary=120000
WHERE id='10101'