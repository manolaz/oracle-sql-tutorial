/*

    Advanced DB - Lab 8: Transaction control

*/

<!-- ----------------

-- Read this https://docs.oracle.com/cd/B14117_01/appdev.101/b10807/13_elems047.htm

-- for Transaction setting - Oracle

---------------- -->



/*-------

-- 1. Read only transaction => for repeatable read (on the same data)

--------*/

-- 1.1. set the transaction to  read only

-- 1.2. read some row(s)

-- 1.3. try to update => error

-- 1.4. start another transaction

-- using  pragma autonomous_transaction;

-- and then try to update on the same row => ok

-- 1.5. read row(s) again => still the same data

-- 1.6. end the current transaction

-- and then read again => updated data is arrived

/*-------

-- 2. Serializable transaction

-------*/

-- 2.1. set the transaction to serializable

-- 2.2. try to update

-- 2.3. start another transaction

-- using  pragma autonomous_transaction;

-- and then try to update on the same row => error



/*-------

-- 3. Serializable transaction, for reading only

-------*/

-- 3.1. set the transaction to serializable

-- 3.2. try to update

-- 3.3. start another transaction

-- using  pragma autonomous_transaction;

-- and then select on the same row => ok



/*-------

-- 4. Read committed transaction, phantom reading

-------*/

-- 4.1. set the transaction to read committed

-- 4.2. try to read many rows

-- 4.3. start another transaction

-- using  pragma autonomous_transaction;

-- and then make a phantom (insert a new record)

-- 4.4. try to read again => phantom



/*-------

-- 5. Hold records

-------*/

-- 5.1. set the transaction to read serializable

-- 5.2.lock a record

-- 5.3. start another transaction

-- using  pragma autonomous_transaction;

-- and then try to update the same record => error



/*-------

-- 6. Hold records in both sides

-------*/

-- 6.1. set the transaction to read serializable

-- 6.2.lock a record

-- 6.3. start another transaction

-- using  pragma autonomous_transaction;

-- and then try to lock the same record before updating it => error





