/*
    Advanced DB - Lab 8: Transaction control
*/

----------------
-- Read this https://docs.oracle.com/cd/B14117_01/appdev.101/b10807/13_elems047.htm
-- for Transaction setting - Oracle
----------------

/*-------
-- 1. Read only transaction => for repeatable read (on the same data)
--------*/

-- 1.1. set the transaction to  read only
set transaction read only;

-- 1.2. read some row(s)
select * from student where id='12345';

-- 1.3. try to update => error
update student set  tot_cred = tot_cred + 1 where id='12345';

-- 1.4. start another transaction
-- using  pragma autonomous_transaction;
-- and then try to update on the same row => ok
declare 
  pragma autonomous_transaction;
begin
    update student set  tot_cred = tot_cred + 1 where id='12345';
    commit;
end;
/

-- 1.5. read row(s) again => still the same data
select * from student where id='12345';

-- 1.6. end the current transacton
commit;

-- and then read again => updated data is arrived
select * from student where id='12345';


/*-------
-- 2. Serializable transaction
-------*/

-- 2.1. set the transaction to serializable
set transaction isolation level serializable;

select * from student where id='12345';

-- 2.2. try to update
update student set  tot_cred = tot_cred - 1 where id='12345';

-- 2.3. start another transaction
-- using  pragma autonomous_transaction;
-- and then try to update on the same row => error
declare 
  pragma autonomous_transaction;
begin
    -- error
    update student set  tot_cred = tot_cred + 1 where id='12345';
    commit;
end;
/

---- new value from this transaction
select * from student where id='12345';

---- end the current transaction
commit;



/*-------
-- 3. Serializable transaction, for reading only
-------*/

-- 3.1. set the transaction to serializable
set transaction isolation level serializable;

select * from student where id='12345';


-- 3.2. try to update
update student set  tot_cred = tot_cred - 1 where id='12345';

-- 3.3. start another transaction
-- using  pragma autonomous_transaction;
-- and then select on the same row => ok
declare 
  pragma autonomous_transaction;
  v_tot_cred INTEGER;
begin
    -- select => ok
    select tot_cred into v_tot_cred from student where id='12345';

    DBMS_OUTPUT.PUT_LINE ('tot_cred: ' || v_tot_cred);
    commit;
end;
/

---- new value from this transaction
select * from student where id='12345';

---- end the current transaction
commit;

/*-------
-- 4. Serializable transaction, for reading-writing
-------*/

-- 4.1. set the transaction to serializable
set transaction isolation level serializable;

select * from student where id='12345';

-- 4.2. start another transaction
-- using  pragma autonomous_transaction;
-- and then update the same row => ok
declare 
  pragma autonomous_transaction;
  v_tot_cred INTEGER;
begin
    -- ok
    update student set  tot_cred = tot_cred + 1 where id='12345';
    commit;
end;
/

---- old value from this transaction
select * from student where id='12345';

---- end the current transaction
commit;

---- new value from other transaction
select * from student where id='12345';



/*-------
-- 5. Read committed transaction, phantom reading
-------*/

-- 5.1. set the transaction to read committed
set transaction isolation level read committed;

-- 5.2. try to read many rows
select * from student where id < '12345';

-- 5.3. start another transaction
-- using  pragma autonomous_transaction;
-- and then make a phantom (insert a new record)
declare 
  pragma autonomous_transaction;
begin
    ---- make a phantom
    insert into student (id, name, dept_name, tot_cred)
    values ('01234', 'New guy 01234', 'Comp. Sci.', 1);
    commit;
end;
/

-- 5.4. try to read again => phantom
select * from student where id<'12345';

---- end the current transaction
commit;


/*-------
-- 6. Hold records
-------*/

-- 6.1. set the transaction to read serializable
set transaction isolation level serializable;

-- 6.2.lock a record
select * from student where id='12345' for update;

-- 6.3. start another transaction
-- using  pragma autonomous_transaction;
-- and then try to update the same record => error
declare 
  pragma autonomous_transaction;
begin
    ---- error: deadlock when this transaction is after the outer transaction
    ---- and the record is locked
    update student set  tot_cred = tot_cred + 1 where id='12345';
    commit;
exception
    when others then
        rollback;
end;
/

---- end the current transaction
---- and release the lock
commit;


/*-------
-- 7. Hold records, another reads
-------*/

-- 7.1. set the transaction to read serializable
set transaction isolation level serializable;

-- 7.2.lock a record
select * from student where id='12345' for update;

-- 7.3. start another transaction
-- using  pragma autonomous_transaction;
-- and then try to read the same data => ok
declare 
  pragma autonomous_transaction;
  v_tot_cred INTEGER;
begin
    select tot_cred into v_tot_cred from student where id='12345';
    DBMS_OUTPUT.PUT_LINE(v_tot_cred);
end;
/

---- end the current transaction
---- and release the lock
commit;



/*-------
-- 8. Hold records in both sides
-------*/

-- 8.1. set the transaction to read serializable
set transaction isolation level serializable;

-- 8.2.lock a record
select * from student where id='12345' for update;

-- 8.3. start another transaction
-- using  pragma autonomous_transaction;
-- and then try to lock the same record before updating it => error
declare 
  pragma autonomous_transaction;
  v_tot_cred INTEGER;
begin
    ---- set an exclusive lock on the record before updating it
    ---- error: deadlock when this transaction is after the outer transaction
    ---- and the record is locked
    select tot_cred into v_tot_cred from student where id='12345' for update;
    DBMS_OUTPUT.PUT_LINE('tot_cred : ' || v_tot_cred);

    -- release the lock
    commit;
    exception
        when others then
            DBMS_OUTPUT.PUT_LINE(SQLCODE || ' : ' || SQLERRM);
            ---- release the lock, just in case
            rollback;
end;
/

---- end the current transaction
---- and release the lock
commit;